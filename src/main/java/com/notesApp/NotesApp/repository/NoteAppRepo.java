package com.notesApp.NotesApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.notesApp.NotesApp.entiy.Notes;

public interface NoteAppRepo extends JpaRepository<Notes, Integer> {

}
