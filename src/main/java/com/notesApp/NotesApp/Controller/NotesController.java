package com.notesApp.NotesApp.Controller;

import java.util.LinkedList;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import com.notesApp.NotesApp.entiy.Notes;
import com.notesApp.NotesApp.repository.NoteAppRepo;
import com.notesApp.NotesApp.service.NoteService;



@Controller
public class NotesController {
	@Autowired
	public NoteService nts;

	@Autowired
	NoteAppRepo ntr;
	
	private static int index;
	
static 	LinkedList<Notes> notesList =new LinkedList<Notes>();
	

	@GetMapping("note-ui")
	public ModelAndView saveNoteView( ModelAndView mv) {
     mv.addObject("notesList",notesList);
		mv.setViewName("/pages/ui.jsp");


		return mv;

	}

@PostMapping("add-note")
public RedirectView addNotes(@RequestParam String subject ,@RequestParam String  notes,HttpServletRequest req){
	
	if(index ==0) {
		index=1;
	}else {
		index = index + 1;
		
	}
	notesList.add(new Notes(index, subject , notes));

	RedirectView rv= new RedirectView();
	rv.setUrl(req.getContextPath() + "/note-ui");
	
	return rv;
	
}

	@PostMapping("save-note")
	public ModelAndView saveNote(@RequestParam String subject ,@RequestParam String  notes,ModelAndView mv) {
		Notes note=new Notes(1,subject,notes);

		mv.setViewName("pages/success.jsp");
		return mv ;	
	}
	@RequestMapping("home")
	public ModelAndView setHome(ModelAndView mv) {


		//mv.addObject("msg")
		mv.setViewName("/pages/home.jsp");
		return mv;

	}
	@GetMapping("delete/{id}")
	public RedirectView deleteNote(@PathVariable Integer id,HttpServletRequest req) {
		notesList.pop();
		RedirectView rv= new RedirectView();
		rv.setUrl(req.getContextPath() + "/note-ui");
		
		return rv;
		
	
		
	}

}
